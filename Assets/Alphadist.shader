﻿Shader "Unlit/Alphadist"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Geometry+500" }
		LOD 100

		Pass
		{
			ZWrite On
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 viewPos : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.viewPos = mul(UNITY_MATRIX_MV, v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float viewZ = -(i.viewPos.z / i.viewPos.w) / 8.0;

				// sample the texture
				fixed4 col = 0.2f;
				col.a = 0.3f;//smoothstep(0.2, 1.0, viewZ);

				//clip(col.a - 0.3);

				return col;
			}
			ENDCG
		}
	}
}
