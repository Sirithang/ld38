﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomTools
{
    [MenuItem("Assets/Create/Icone from Selected Asset")]
    static public void CreateIcone()
    {
        Texture2D preview = AssetPreview.GetAssetPreview(Selection.activeObject);
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);

        string fileName = Selection.activeObject.name + "_preview.png";
        path = System.IO.Path.GetDirectoryName(path);
        path = AssetDatabase.GenerateUniqueAssetPath(path + "/" + fileName);
        System.IO.File.WriteAllBytes(path, preview.EncodeToPNG());
        AssetDatabase.Refresh();
    }
}
