﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : LevelCell
{
    public string spawnerType;
    public int maxCount = 1;
    public MonoBehaviour toSpawn;

    protected MonoBehaviour _spawned;

    public override string GetCellType()
    {
        return spawnerType;
    }

    public override void Created()
    {
        Respawn();
    }

    void Update()
    {
        if(EditorController.instance == null && _spawned == null)
        {
            Respawn();
        }
    }

    void Respawn()
    {
        int x, y, z;
        Level.instance.WorldToCoordinate(transform.position, out x, out y, out z);

        _spawned = Instantiate(toSpawn, transform, false);
        _spawned.transform.localPosition = Vector3.zero;
    }
}
