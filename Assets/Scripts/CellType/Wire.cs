﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wire : LevelCell
{
    public string wireType;
    [HideInInspector]
    public int activationCount = 0;

    public override string GetCellType()
    {
        return wireType;
    }
}
