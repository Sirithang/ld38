﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorButton: LevelCell
{
    int _pressings = 0;
    bool _triggered = false;


    public override string GetCellType()
    {
        return "Button";
    }


    void OnTriggerEnter(Collider other)
    {
        _pressings += 1;
        if (!_triggered)
            Trigger(true);
    }

    void Trigger(bool triggered)
    {
        _triggered = triggered;

        List<LevelCell> explored = new List<LevelCell>();
        int x, y, z;
        Level.instance.WorldToCoordinate(transform.position, out x, out y, out z);
        RecursiveExploration(x, y, z, explored, triggered ? 1 : -1);
    }

    //activated = 1 when activating, -1 when deactivating
    void RecursiveExploration(int x, int y, int z, List<LevelCell> explored, int activated)
    {
        //we go throught all the connected wire and turn them "on"
        int[,] offsets =
        {
            {1,0,0},
            {0,1,0},
            {0,0,1},
            {-1,0,0},
            {0,-1,0},
            {0,0,-1}
        };

        for (int i = 0; i < 6; ++i)
        {
            int lx = x + offsets[i, 0];
            int ly = y + offsets[i, 1];
            int lz = z + offsets[i, 2];

            List<LevelCell> cells = Level.instance.GetCell(lx, ly, lz);
            if (cells == null)
                continue;

            for(int j = 0; j < cells.Count; ++j)
            {
                Wire w = cells[j] as Wire;
                if (w == null || explored.Contains(w))
                    continue;

                explored.Add(w);
                w.activationCount += activated;
                RecursiveExploration(lx, ly, lz, explored, activated);
            }
        }
    }

    void OnTriggerExit()
    {
        _pressings -= 1;
        if(_pressings == 0)
        {
            Trigger(false);
        }
    }
}
