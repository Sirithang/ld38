﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : LevelCell
{
    public override string GetCellType()
    {
        return "StartPoint";
    }
}
