﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : LevelCell
{
    public override string GetCellType()
    {
        return "Door";
    }

    int _x, _y, _z;

    public override void Created()
    {
        Level.instance.WorldToCoordinate(transform.position, out _x, out _y, out _z);
    }

    void Update()
    {
        if(EditorController.instance == null)
            transform.GetChild(0).gameObject.SetActive(!CheckActivation());
    }

    bool CheckActivation()
    {
       int[,] offsets =
       {
            {1,0,0},
            {0,1,0},
            {0,0,1},
            {-1,0,0},
            {0,-1,0},
            {0,0,-1}
        };

        for (int i = 0; i < 6; ++i)
        {
            int lx = _x + offsets[i, 0];
            int ly = _y + offsets[i, 1];
            int lz = _z + offsets[i, 2];

            List<LevelCell> cells = Level.instance.GetCell(lx, ly, lz);
            if (cells == null)
                continue;

            for (int j = 0; j < cells.Count; ++j)
            {
                Wire w = cells[j] as Wire;
                if (w != null && w.activationCount > 0)
                    return true;
            }
        }

        return false;
    }
}
