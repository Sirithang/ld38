﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPoint : LevelCell
{
    [HideInInspector]
    public bool isActive = false;

    public override string GetCellType()
    {
        return "EndPoint";
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerController>() != null && isActive)
        {
            Debug.Log("VICTORY MOFO");
        }
    }
}
