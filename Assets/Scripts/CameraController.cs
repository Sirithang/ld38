﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform target;
    public float distance = 6.0f;

    protected Vector3 _refVelocity;

	// Use this for initialization
	void Start ()
    {
        _refVelocity = Vector3.zero;
	}

	void LateUpdate ()
    {
        Vector3 pos;
        Quaternion rot;

        GetTargetPositionAndRotation(out pos, out rot);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, 270.0f * Time.deltaTime);
        transform.position = Vector3.SmoothDamp(transform.position, pos, ref _refVelocity, 0.2f);
	}

    void GetTargetPositionAndRotation(out Vector3 position, out Quaternion rotation)
    {
        Vector3 lookVector = PlayerController.instance.controlType == PlayerController.ControlType.SIDE ? Vector3.forward : Vector3.down;

        position = target.position - lookVector * distance;
        rotation = Quaternion.LookRotation(lookVector);
    }
}
