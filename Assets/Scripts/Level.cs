﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Level : MonoBehaviour
{
    static protected Level s_instance = null;
    static public Level instance { get { return s_instance; } }

    public StartPoint startPoint { get { return _startPoint; } }

    public int width { get { return _width; } }
    public int height { get { return _height; } }
    public int depth { get { return _depth; } }

    int _width = 30;
    int _height = 16;
    int _depth = 30;

    List<LevelCell>[] _level;

    StartPoint _startPoint;

    void Awake()
    {
        s_instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        if(EditorController.instance != null)
        {//we are in the editor, so do nothing

        }
        else
        {//this is a play scene, read the data needed to create that level from the DataPasser
            MemoryStream stream = Utility.DecompressData(System.Convert.FromBase64String(DataPasser.levelString));
            BinaryReader reader = new BinaryReader(stream);

            Deserialize(reader);

            reader.Close();

            PlayerController.instance.Spawn();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public int CoordinateToIndex(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0 || x >= _width || y >= _height || z >= _depth)
            return -1;

        return y * _width * _depth + x * _depth + z;
    }

    public Vector3 CoordinateToLocalPosition(int x, int y, int z)
    {
        return new Vector3(x, y, z);
    }

    public Vector3 CoordinateToLocalPositionCenter(int x, int y, int z)
    {
        return new Vector3(x+0.5f, y+0.5f, z+0.5f);
    }

    public Vector3 CoordinateToWorldPosition(int x, int y, int z)
    {
        return transform.TransformPoint(CoordinateToLocalPosition(x, y, z));
    }

    public Vector3 CoordinateToWorldPositionCenter(int x, int y, int z)
    {
        return transform.TransformPoint(CoordinateToLocalPositionCenter(x, y, z));
    }



    public bool WorldToCoordinate(Vector3 worldPos, out int x, out int y, out int z)
    {
        Vector3 local = transform.InverseTransformPoint(worldPos);

        x = Mathf.FloorToInt(local.x);
        y = Mathf.FloorToInt(local.y);
        z = Mathf.FloorToInt(local.z);

        return (x >= 0 && y >= 0 && z >= 0 && x < _width && y < _height && z < _depth);

    }

    public void SwitchView(PlayerController.ControlType controlType, int x, int y, int z)
    {
        if(controlType == PlayerController.ControlType.SIDE)
        {
            SwitchMaterialTop(y, true);
            SwitchMaterialSide(z, false);
        }
        else
        {
            SwitchMaterialSide(z, true);
            SwitchMaterialTop(y, false);
        }
    }

    public void SwitchMaterialTop(int separtorY, bool opaque)
    {
        for (int y = _height - 1; y > separtorY; --y)
        {
            for (int x = 0; x < _width; ++x)
            {
                for (int z = 0; z < _depth; ++z)
                {
                    List<LevelCell> cell = _level[CoordinateToIndex(x, y, z)];
                    if (cell != null)
                    {
                        for (int i = 0; i < cell.Count; ++i)
                            cell[i].SwitchMaterial(opaque);
                    }
                }
            }
        }
    }

    public void SwitchMaterialSide(int separtorZ, bool opaque)
    {
        for (int z = 0; z < separtorZ; ++z)
        {
            for (int y = 0; y < _height; ++y)
            {
                for (int x = 0; x < _width; ++x)
                {
                    List<LevelCell> cell = _level[CoordinateToIndex(x, y, z)];
                    if (cell != null)
                    {
                        for(int  i = 0; i < cell.Count; ++i)
                            cell[i].SwitchMaterial(opaque);
                    }
                }
            }
        }
    }

    public void BackToEditor()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Editor", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public LevelCell AddToCell(int x, int y, int z, LevelCell prefab, float angle)
    {
        if (prefab == null)
            return null;

        int idx = CoordinateToIndex(x, y, z);
        if (_level[idx] == null)
            _level[idx] = new List<LevelCell>();

        LevelCell c = Instantiate(prefab, CoordinateToLocalPositionCenter(x, y, z), Quaternion.Euler(0,angle,0), transform);
        _level[idx].Add(c);

        return c;
    }

    public void RemoveFromCell(int x, int y, int z, LevelCell prefab, float angle)
    {
        int i = FindCell(x, y, z, prefab, angle);

        if (i == -1)
            return;

        int idx = CoordinateToIndex(x, y, z);
        Destroy(_level[idx][i].gameObject);
        _level[idx].RemoveAt(i);
    }

    //if angle < 0.0f, mean it don't matter, any cell of given prefab type is valid.
    //if angle >= 0.0f, will only return the one that match the angle 
    public int FindCell(int x, int y, int z, LevelCell prefab, float angle)
    {
        int idx = CoordinateToIndex(x, y, z);
        if (_level[idx] == null)
            return -1;

        string searchedType = prefab.GetCellType();

        for (int i = 0; i < _level[idx].Count; ++i)
        {
            if (_level[idx][i].GetCellType() == searchedType && (angle < -0.01f || Mathf.Approximately(_level[idx][i].transform.rotation.eulerAngles.y, angle)))
            {
                return i;
            }
        }

        return -1;
    }

    public List<LevelCell> GetCell(int x, int y, int z)
    {
        int idx = CoordinateToIndex(x, y, z);
        if (idx == -1)
            return null;
        return _level[idx];
    }

    public void Fill(int width, int height, int depth)
    {
        _width = width;
        _height = height;
        _depth = depth;

        _level = new List<LevelCell>[_width * _height * _depth];
    }

    public void Serialize(BinaryWriter writer)
    {
        writer.Write(_width);
        writer.Write(_height);
        writer.Write(_depth);

        for (int y = 0; y < _height; ++y)
        {
            for (int x = 0; x < _width; ++x)
            {
                for (int z = 0; z < _depth; ++z)
                {
                    int idx = CoordinateToIndex(x, y, z);
                    int count = (_level[idx] == null ? 0 : _level[idx].Count);
                    writer.Write(count);
                    for(int i = 0; i < count; ++i)
                    {
                        writer.Write(_level[idx][i].GetCellType());
                        writer.Write(_level[idx][i].transform.eulerAngles.y);
                    }
                }
            }
        }
    }

    public void Deserialize(BinaryReader reader)
    {
        Fill(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
        BlockList bl = BlockList.instance;

        for (int y = 0; y < _height; ++y)
        {
            for (int x = 0; x < _width; ++x)
            {
                for (int z = 0; z < _depth; ++z)
                {
                    int count = reader.ReadInt32();
                    if (count == 0)
                        continue;

                    int idx = CoordinateToIndex(x, y, z);
                    for (int i = 0; i < count; ++i)
                    {
                        string t = reader.ReadString();
                        float angle = reader.ReadSingle();

                        LevelCell c = AddToCell(x, y, z, bl.GetCell(t), angle);

                        if (t == "StartPoint")
                            _startPoint = c as StartPoint;
                    }
                }
            }
        }
    }
}

public abstract class LevelCell : MonoBehaviour
{
    public abstract string GetCellType();

    protected Renderer _renderer;

    public Material opaqueMaterial;
    public Material transparentMaterial;
    [Header("Editor Data")]
    public Material editorMaterial;
    public Sprite previewIcone;
    public bool canBeStacked = false;

    public virtual void Start()
    {
        _renderer = GetComponentInChildren<Renderer>();

        if (EditorController.instance != null)
        {//we're in editor scene, so setup the cell for editor display
            SetEditorMaterial();
            CreateSprite();
        }
        else
        {//this is non editor, call the creation function
            Created();
        }
    }

    public virtual void Created()
    {

    }

    public void SwitchMaterial(bool opaque)
    {
        if (_renderer == null)
            return;

        _renderer.sharedMaterial = opaque ? opaqueMaterial : transparentMaterial;
    }

    //if this is a pure gameplay object, will create a sprite
    void CreateSprite()
    {
        if (opaqueMaterial != null || previewIcone == null)
            return;

        GameObject display = new GameObject("dispSprite");
        display.transform.SetParent(transform, false);

        display.AddComponent<BadBillboard>();
        display.AddComponent<SpriteRenderer>().sprite = previewIcone;
    }

    public void SetEditorMaterial()
    {
        if (_renderer == null)
            return;

        _renderer.sharedMaterial = editorMaterial;
    }
}