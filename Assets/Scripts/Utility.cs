﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;

public class Utility
{
    //Carefull this assume value will never be more than once the range away.
    //e.g. calling it for 10, 2, 5 will fail and return 6
    static public int Wrap(int value, int min, int max)
    {
        int diff = max - min + 1;
        if (value < min)
        {
            return value + diff;
        }
        else if (value > max)
            return value - diff;

        return value;
    }


    static public byte[] CompressData(byte[] input)
    {
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(output, CompressionMode.Compress))
        {
            dstream.Write(input, 0, input.Length);
        }

        byte[] ret = output.ToArray();

        return ret;
    }

    public static void CopyTo(Stream input, Stream output)
    {
        byte[] buffer = new byte[50 * 1024 * 1024]; // Fairly arbitrary size
        int bytesRead;

        while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, bytesRead);
        }
    }

    static public MemoryStream DecompressData(byte[] data)
    {
        MemoryStream input = new MemoryStream(data);
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
        {
            CopyTo(dstream, output);
        }
        output.Position = 0;

        return output;
    }
}
