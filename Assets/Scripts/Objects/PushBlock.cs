﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushBlock : MonoBehaviour
{
    float _pushTimer = 0.0f;
    float _sinceLastPush = 0.0f;
    bool _pushed = false;
    Vector3 _pushDirection = Vector3.zero;
    bool _pushable = true;

    Rigidbody _rigidbody;

    void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void Update ()
    {
        if (_pushed)
        {
            if (_pushTimer > 0.3f)
            {
                int x, y, z;
                Level.instance.WorldToCoordinate(transform.position, out x, out y, out z);

                Vector3 current = Level.instance.CoordinateToWorldPositionCenter(x, y, z);

                x += Mathf.RoundToInt(_pushDirection.x);
                z += Mathf.RoundToInt(_pushDirection.z);

                Vector3 pos = Level.instance.CoordinateToWorldPositionCenter(x, y, z);

                if(CastToTarget(current, pos))
                {
                    StartCoroutine(MoveTo(pos));
                }

                _pushed = false;
                _pushTimer = 0.0f;
                _pushDirection = Vector3.zero;
            }
            else
            {
                _sinceLastPush += Time.deltaTime;
                if (_sinceLastPush > 0.3f)
                {
                    _pushTimer = 0.0f;
                    _pushDirection = Vector3.zero;
                }
            }
        }
	}

    IEnumerator MoveTo(Vector3 position)
    {
        _pushable = false;
        _rigidbody.isKinematic = true;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        Vector2 target2D = new Vector2(position.x, position.z);
        Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);

        while(Vector2.Distance(target2D, pos2D) > 0.001f)
        {
            yield return null;
            pos2D.Set(_rigidbody.position.x, _rigidbody.position.z);
            pos2D = Vector2.MoveTowards(pos2D, target2D, 10.0f * Time.deltaTime);

            _rigidbody.position = new Vector3(pos2D.x, transform.position.y, pos2D.y);
        }

        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        _rigidbody.isKinematic = false;
        _pushable = true;
    }

    bool CastToTarget(Vector3 origin, Vector3 position)
    {
        origin += Vector3.up * 0.25f;
        position += Vector3.up * 0.25f;

        RaycastHit info;
        Vector3 pos = _rigidbody.position;
        _rigidbody.position = origin;

        Vector3 dir = position - origin;
        bool ret = _rigidbody.SweepTest(dir, out info, dir.magnitude);

        _rigidbody.position = pos;
        return !ret;
    }

    public void Pushed(Vector3 direction)
    {
        if (!_pushable)
            return;

        _pushed = true;
        if (direction == _pushDirection)
        {
            _pushTimer += Time.deltaTime;
        }
        else
        {
            _pushTimer = 0.0f;
            _pushDirection = direction;
        }

        _sinceLastPush = 0.0f;
    }
}
