﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    static PlayerController s_instance;
    static public PlayerController instance { get { return s_instance; } }

    public enum ControlType
    {
        SIDE,
        TOP
    }

    public ControlType controlType { get { return _controlType; } }

    CharacterController _cc;
    ControlType _controlType;
    Vector3 _velocity;

    bool _controllable = true;

    int _x, _y, _z;

    const float speed = 7.0f;
    const float acceleration = 25.0f;
    const float gravity = 10.0f;
    const float jumpStrength = 8.0f;

    void Awake()
    {
        s_instance = this;
        _cc = GetComponent<CharacterController>();
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        
    }

    public void Spawn()
    {
        transform.position = Level.instance.startPoint.transform.position + Vector3.up * 1.1f;
        Level.instance.WorldToCoordinate(transform.position, out _x, out _y, out _z);
        gameObject.SetActive(true);

        _controlType = ControlType.TOP;
        SwitchControlType();
    }

    // Update is called once per frame
    void Update()
    {
        _velocity.y -= gravity * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            SwitchControlType();
        }

        if (_controllable)
        {
            if (_controlType == ControlType.SIDE)
                SideMovment(ref _velocity);
            else
                TopMovement(ref _velocity);
        }

        CollisionFlags cflags = _cc.Move(_velocity * Time.deltaTime);

        if ((cflags & CollisionFlags.Above) != 0 && _velocity.y > 0.01f)
        {
            _velocity.y = 0;
        }

        int x, y, z;
        Level.instance.WorldToCoordinate(transform.position, out x, out y, out z);

        if(_controlType == ControlType.TOP && _cc.isGrounded)
        {//if we are grounded on the top, we check we didn't fall since last time we set the transparency
            if(y != _y)
            {//we changed height, so recompute the 
                Level.instance.SwitchMaterialTop(y, false);
            }
        }

        if (_cc.isGrounded)
        {
            _x = x;
            _y = y;
            _z = z;
        }
    }

    void SwitchControlType()
    {
        _controlType = _controlType == ControlType.TOP ? ControlType.SIDE : ControlType.TOP;

        _velocity.x = 0;
        _velocity.z = 0;
        if(_velocity.y > 0) _velocity.y = 0;

        Level.instance.SwitchView(_controlType, _x, _y, _z);
    }

    IEnumerator MoveTo(Vector3 position)
    {
        _controllable = false;

        while(Vector3.Distance(position, transform.position) > 0.001f)
        {
            position.y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, position, 5.0f * Time.deltaTime);
            yield return null;
        }

        transform.position = position;

        _controllable = true;
    }

    void SideMovment(ref Vector3 velocity)
    {
        velocity.z = 0;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            velocity.x = Mathf.MoveTowards(velocity.x, -speed, Time.deltaTime * acceleration);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            velocity.x = Mathf.MoveTowards(velocity.x, speed, Time.deltaTime * acceleration);
        }
        else
            velocity.x = Mathf.MoveTowards(velocity.x, 0, 14.0f * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_cc.isGrounded)
            {
                velocity.y = jumpStrength;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Space) && velocity.y > 0.01f)
        {
            velocity.y *= 0.5f;
        }
    }

    void TopMovement(ref Vector3 velocity)
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            velocity.x = Mathf.MoveTowards(velocity.x, -speed, Time.deltaTime * acceleration);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            velocity.x = Mathf.MoveTowards(velocity.x, speed, Time.deltaTime * acceleration);
        }
        else
            velocity.x = Mathf.MoveTowards(velocity.x, 0, 14.0f * Time.deltaTime);

        if (Input.GetKey(KeyCode.UpArrow))
        {
            velocity.z = Mathf.MoveTowards(velocity.z, speed, Time.deltaTime * acceleration);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            velocity.z = Mathf.MoveTowards(velocity.z, -speed, Time.deltaTime * acceleration);
        }
        else
            velocity.z = Mathf.MoveTowards(velocity.z, 0, 14.0f * Time.deltaTime);
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        PushBlock bl = hit.collider.GetComponent<PushBlock>();

        if(bl!= null)
        {
            bl.Pushed(-hit.normal);
        }


        float dot = Vector3.Dot(_velocity, hit.normal);
        if (dot > 0.0f)
            return;
        _velocity -= dot * hit.normal;
    }
}
