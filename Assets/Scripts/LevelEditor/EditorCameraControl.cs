﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorCameraControl : MonoBehaviour
{
    Vector3 _previousMousePosition;
    float _rotationY;

	// Use this for initialization
	void Start ()
    {
        transform.position =new Vector3(-1,1,-1) * 20.0f;
        transform.LookAt(Vector3.zero);

        _previousMousePosition = Input.mousePosition;
    }
	
	// Update is called once per frame
	void Update ()
    {
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel");

        if (Input.GetMouseButton(1))
        {
            RotateView();
        }

        if(Input.GetMouseButton(2))
        {
            TranslateView();
        }

        if(mouseScroll != 0.0f)
        {
            MoveAlongView(mouseScroll * 300.0f * Time.deltaTime);
        }

        _previousMousePosition = Input.mousePosition;
    }


    void RotateView()
    {
        Vector3 deltaMouse = Input.mousePosition - _previousMousePosition;

        transform.Rotate(Vector3.right, -deltaMouse.y * 25.0f * Time.deltaTime, Space.Self);
        transform.Rotate(Vector3.up, deltaMouse.x * 25.0f * Time.deltaTime, Space.World);
    }

    void TranslateView()
    {
        Vector3 deltaMouse = Input.mousePosition - _previousMousePosition;

        transform.Translate(new Vector3(-deltaMouse.x * Time.deltaTime * 2.0f, -deltaMouse.y * Time.deltaTime * 2.0f, 0), Space.Self);
    }

    void MoveAlongView(float value)
    {
        transform.Translate(Vector3.forward * value, Space.Self);
    }
}
