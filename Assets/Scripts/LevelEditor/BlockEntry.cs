﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockEntry : MonoBehaviour
{
    static BlockEntry s_currentSelected = null;

    public Button button;
    public Image icone;
    public Text typeName;

    protected LevelCell _storedCell;

    public void Init(LevelCell prefab)
    {
        _storedCell = prefab;

        icone.sprite = prefab.previewIcone;
        typeName.text = prefab.GetCellType();
        button.enabled = true;
    }

    public void Selected()
    {
        if (s_currentSelected != null) s_currentSelected.Unselect();

        EditorController.instance.currentPainted = _storedCell;
        button.enabled = false;
        
        s_currentSelected = this;
    }

    public void Unselect()
    {
        button.enabled = true;
        s_currentSelected = null;
    }
}
