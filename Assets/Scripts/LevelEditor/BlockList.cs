﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="BlockList", menuName = "Kub/BlockList")]
public class BlockList : ScriptableObject
{
    static BlockList s_instance = null;
    static public BlockList instance { get { if (s_instance == null) Load();  return s_instance; } }

    public LevelCell[] blocks;

    protected Dictionary<string, LevelCell> _lookupTable;

    static void Load()
    {
        s_instance = Resources.Load<BlockList>("BlockList");
        s_instance.Init();
    }


    void Init()
    {
        _lookupTable = new Dictionary<string, LevelCell>();

        for(int i = 0; i < blocks.Length; ++i)
        {
            if(_lookupTable.ContainsKey(blocks[i].GetCellType()))
            {
                Debug.LogError("Duplicate cell type " + blocks[i].GetCellType());
                continue;
            }

            _lookupTable[blocks[i].GetCellType()] = blocks[i];
        }
    }

    public LevelCell GetCell(string cellType)
    {
        return _lookupTable[cellType];
    }
}
