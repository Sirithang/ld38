﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmNewLevel : MonoBehaviour
{
    public void ValidateNewLevel()
    {
        EditorController.instance.CreateNewLevel();
    }

    public void CancelNewLevel()
    {
        EditorController.instance.CancelNewLevelCreation();
    }
}
