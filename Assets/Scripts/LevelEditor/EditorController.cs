﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.UI;

public class EditorController : MonoBehaviour
{
    public enum EditMode
    {
        ROW,
        COLUMN
    }

    static EditorController s_Instance = null;
    static public EditorController instance { get { return s_Instance; } }

    public EditorCameraControl cameraControl;

    public Material gridMaterial;
    public Material activeGridMaterial;

    public MeshRenderer editorMarker;
    public GameObject sideArrow;

    public Level currentLevel;

    [Header("UI")]
    public GameObject sideBar;
    public ConfirmNewLevel confirmNewLevel;
    public LevelCreatePopup levelInfo;

    public Text editionModeText;

    public RectTransform blockEntryList;
    public BlockEntry blockEntryPrefab;

    public LevelCell currentPainted {  get { return _paintedPrefab; } set { _paintedPrefab = value; SetupPreviewCell(); } }
    public float currentAngle { get { return _currentAngle; } }

    protected MeshRenderer _boundRenderer;
    protected MeshFilter _boundFilter;

    public BlockList _blockList;

    protected GameObject _editionColumnGrid;
    protected GameObject _editionRowGrid;

    protected LevelCell _previewCell = null;

    protected EditMode _currentEditMode = EditMode.ROW;
    protected Plane _currentEditionPlane;
    protected int _currentRowEdited = 0;
    protected int _currentColumnEdited = 0;
    protected float _currentAngle = 0.0f;

    protected bool _canEdit = false;
    protected LevelCell _paintedPrefab;

    protected int _hoveredX, _hoveredY, _hoveredZ;

    protected string _editorFilepath;
    protected string _currentLevelString = "";

    void Awake()
    {
        s_Instance = this ;
        _editorFilepath = Application.persistentDataPath + "/editedlevel.bin";
    }

    void OnDestroy()
    {
        s_Instance = null;
    }

    void Start()
    {
        if (File.Exists(_editorFilepath))
        {
            Load();
        }
        else
        {
            levelInfo.gameObject.SetActive(true);
            sideBar.gameObject.SetActive(false);

            cameraControl.enabled = false;

            _paintedPrefab = Resources.Load<LevelCell>("CellType/Blocker");

            _hoveredX = -1;
            _hoveredY = -1;
            _hoveredZ = -1;
            editorMarker.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (!_canEdit)
            return;

        GetCurrentHovered();

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            SwitchEditionMode();
        }

        if (Input.GetKeyDown(KeyCode.E))
            ChangeCurrentEdited(-1);
        if (Input.GetKeyDown(KeyCode.R))
            ChangeCurrentEdited(1);
        if (Input.GetKeyDown(KeyCode.T))
            Rotate();

        if(Input.GetKeyDown(KeyCode.S))
        {
            if(Input.GetKey(KeyCode.LeftControl))
            {
                Save();
            }
        }


        if (Input.GetMouseButton(0))
        {
            Paint(Input.GetKey(KeyCode.LeftControl));
        }

        if (_hoveredX != -1)
        {
            editorMarker.gameObject.SetActive(true);
            editorMarker.sharedMaterial.color = Input.GetKey(KeyCode.LeftControl) ? Color.red : Color.green;
            editorMarker.transform.position = Level.instance.CoordinateToWorldPositionCenter(_hoveredX, _hoveredY, _hoveredZ);
        }
        else
        {
            editorMarker.gameObject.SetActive(false);
        }
    }

    public void DisplayNewLevelPopup()
    {
        _canEdit = false;
        confirmNewLevel.gameObject.SetActive(true);
    }

    public void CancelNewLevelCreation()
    {
        _canEdit = true;
        confirmNewLevel.gameObject.SetActive(false);
    }

    public void CreateNewLevel()
    {
        File.Delete(_editorFilepath);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Editor", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public void Test()
    {
        DataPasser.isPreview = true;
        Save();
        DataPasser.levelString = _currentLevelString;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Play", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public void Save()
    {
        MemoryStream stream = new MemoryStream();
        BinaryWriter writer = new BinaryWriter(stream);

        currentLevel.Serialize(writer);

        byte[] data = stream.ToArray();

        data = Utility.CompressData(data);

        _currentLevelString = System.Convert.ToBase64String(data);

        File.WriteAllBytes(_editorFilepath, data);

        writer.Close();
    }

    

    public void Load()
    {
        MemoryStream output = Utility.DecompressData(File.ReadAllBytes(_editorFilepath));
        BinaryReader reader = new BinaryReader(output);

        GameObject obj = new GameObject("Level");
        currentLevel = obj.AddComponent<Level>();
        currentLevel.Deserialize(reader);

        reader.Close();

        StartEdit(); 
    }

    void GetCurrentHovered()
    {
        int x, y, z;
        float enter;

        _hoveredX = _hoveredY = _hoveredZ = -1;

        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (_currentEditionPlane.Raycast(r, out enter))
        {
            Vector3 worldPos = r.GetPoint(enter);
            worldPos += _currentEditionPlane.normal * 0.1f;
            if (Level.instance.WorldToCoordinate(worldPos, out x, out y, out z))
            {
                _hoveredX = x;
                _hoveredY = y;
                _hoveredZ = z;
            }
        }
    }

    void Paint(bool delete)
    {
        if (_hoveredX == -1)
            return;

        //if it can't be stacked, search with -1 as angle, tell the Level that it need to return ANY version of that prefab, ignoring its rotation
        int index = Level.instance.FindCell(_hoveredX, _hoveredY, _hoveredZ, _paintedPrefab, _paintedPrefab.canBeStacked ? currentAngle : -1.0f);

        if ((delete && index == -1) || (!delete && index != -1))
            return; //we either try to delete a cell that is not here or add one that is already here

        if (delete)
        {
            Level.instance.RemoveFromCell(_hoveredX, _hoveredY, _hoveredZ, _paintedPrefab, currentAngle);
        }
        else
        { 
            Level.instance.AddToCell(_hoveredX, _hoveredY, _hoveredZ, _paintedPrefab, currentAngle);
        }
    }

    public void CreateLevel()
    {
        GameObject obj = new GameObject("Level");
        currentLevel = obj.AddComponent<Level>();
        currentLevel.Fill(int.Parse(levelInfo.width.text), int.Parse(levelInfo.height.text), int.Parse(levelInfo.depth.text));

        StartEdit();
    }

    void StartEdit()
    {
        levelInfo.gameObject.SetActive(false);
        sideBar.gameObject.SetActive(true);

        cameraControl.enabled = true;

        CreateBoundMesh();
        CreateEditionGrid();
        SetRowEdited(0);

        _blockList = BlockList.instance;
        for (int i = 0; i < _blockList.blocks.Length; ++i)
        {
            BlockEntry entry = Instantiate(blockEntryPrefab, blockEntryList);
            entry.Init(_blockList.blocks[i]);
        }

        sideArrow.transform.position = new Vector3(currentLevel.width * 0.5f, currentLevel.height * 0.5f, -5.0f);

        _canEdit = true;
    }

    void SetColumnEdited(int column)
    {
        _editionRowGrid.SetActive(false);
        _editionColumnGrid.SetActive(true);

        _editionColumnGrid.transform.position = Level.instance.CoordinateToWorldPosition(column, 0, 0);
        _currentColumnEdited = column;

        _currentEditionPlane = new Plane(Vector3.right, _editionColumnGrid.transform.position);
    }

    void SetRowEdited(int row)
    {
        _editionRowGrid.SetActive(true);
        _editionColumnGrid.SetActive(false);

        _editionRowGrid.transform.position = Level.instance.CoordinateToWorldPosition(0, row, 0);
        _currentRowEdited = row;

        _currentEditionPlane = new Plane(Vector3.up, _editionRowGrid.transform.position);
    }

    public void SetupPreviewCell()
    {
        if (_previewCell != null)
            Destroy(_previewCell.gameObject);

        _previewCell = Instantiate(_paintedPrefab, editorMarker.transform, false);
        _previewCell.transform.localPosition = Vector3.zero;
        _previewCell.transform.localRotation = Quaternion.Euler(0, currentAngle, 0);
    }

    public void Rotate()
    {
        _currentAngle = (_currentAngle + 90.0f) % 360.0f;
        SetupPreviewCell();
    }

    public void SwitchEditionMode()
    {
        if(_currentEditMode == EditMode.COLUMN)
        {
            _currentEditMode = EditMode.ROW;
            SetRowEdited(_currentRowEdited);
            editionModeText.text = "Row";
        }
        else
        {
            _currentEditMode = EditMode.COLUMN;
            SetColumnEdited(_currentColumnEdited);
            editionModeText.text = "Column";
        }
    }

    public void ChangeCurrentEdited(int direction)
    {
        if (_currentEditMode == EditMode.COLUMN)
        {
            SetColumnEdited(Utility.Wrap(_currentColumnEdited + direction, 0, Level.instance.depth - 1));
        }
        else
        {
            SetRowEdited(Utility.Wrap(_currentRowEdited + direction, 0, Level.instance.height - 1));
        }
    }

    void CreateEditionGrid()
    {
        _editionColumnGrid = new GameObject("EditionColumnGrid");
        _editionRowGrid = new GameObject("EditionRowGrid");

        MeshRenderer rend = _editionColumnGrid.AddComponent<MeshRenderer>();
        rend.material = activeGridMaterial;

        rend = _editionRowGrid.AddComponent<MeshRenderer>();
        rend.material = activeGridMaterial;

        // === ROW Grid
        MeshFilter filter = _editionRowGrid.AddComponent<MeshFilter>();
        filter.sharedMesh = new Mesh();

        Mesh m = filter.sharedMesh;
        m.vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(currentLevel.width, 0, 0),
            new Vector3(currentLevel.width, 0, currentLevel.depth),
            new Vector3(0, 0, currentLevel.depth)
        };

        m.normals = new Vector3[]
        {
            Vector3.up,Vector3.up,Vector3.up,Vector3.up
        };

        m.uv = new Vector2[]
        {
            new Vector2(0,0), new Vector2(currentLevel.width, 0),new Vector2(currentLevel.width, currentLevel.depth),new Vector2(0, currentLevel.depth)
        };

        m.triangles = new int[]
        {
            0,2,1, 0,3,2
        };

        // === COLUMN Grid
        filter = _editionColumnGrid.AddComponent<MeshFilter>();
        filter.sharedMesh = new Mesh();

        m = filter.sharedMesh;
        m.vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(0, 0, currentLevel.depth),
            new Vector3(0, currentLevel.height, currentLevel.depth),
            new Vector3(0, currentLevel.height, 0),
        };

        m.normals = new Vector3[]
        {
           Vector3.right,Vector3.right,Vector3.right,Vector3.right
        };

        m.uv = new Vector2[]
        {
            new Vector2(0,0), new Vector2(currentLevel.depth, 0),new Vector2(currentLevel.depth, currentLevel.height),new Vector2(0, currentLevel.height)
        };

        m.triangles = new int[]
        {
            0,2,1, 0,3,2
        };

        _editionColumnGrid.SetActive(false);
    }

    void CreateBoundMesh()
    {
        GameObject bound = new GameObject("BoundDisplay");
        _boundRenderer = bound.AddComponent <MeshRenderer>();
        _boundFilter = bound.AddComponent<MeshFilter>();
        Mesh m = new Mesh();

        _boundRenderer.sharedMaterial = gridMaterial;
        _boundFilter.sharedMesh = m;

        m.vertices = new Vector3[]
        {
            //bottom
            new Vector3(0,0,0),
            new Vector3(currentLevel.width, 0, 0),
            new Vector3(currentLevel.width, 0, currentLevel.depth),
            new Vector3(0, 0, currentLevel.depth),

            //top
            new Vector3(0,currentLevel.height,0),
            new Vector3(currentLevel.width, currentLevel.height, 0),
            new Vector3(currentLevel.width, currentLevel.height, currentLevel.depth),
            new Vector3(0, currentLevel.height, currentLevel.depth),

            //left
            new Vector3(0,0,0),
            new Vector3(0, 0, currentLevel.depth),
            new Vector3(0, currentLevel.height, currentLevel.depth),
            new Vector3(0, currentLevel.height, 0),

            //right
            new Vector3(currentLevel.width,0,0),
            new Vector3(currentLevel.width, 0, currentLevel.depth),
            new Vector3(currentLevel.width, currentLevel.height, currentLevel.depth),
            new Vector3(currentLevel.width, currentLevel.height, 0),

             //back
            new Vector3(0,0,0),
            new Vector3(currentLevel.width, 0, 0),
            new Vector3(currentLevel.width, currentLevel.height, 0),
            new Vector3(0, currentLevel.height, 0),

              //front
            new Vector3(0,0,currentLevel.depth),
            new Vector3(currentLevel.width, 0, currentLevel.depth),
            new Vector3(currentLevel.width, currentLevel.height, currentLevel.depth),
            new Vector3(0, currentLevel.height, currentLevel.depth)
        };

        m.normals = new Vector3[]
        {
            Vector3.down, Vector3.down, Vector3.down,Vector3.down,
            Vector3.up,Vector3.up,Vector3.up,Vector3.up,
            Vector3.left,Vector3.left,Vector3.left,Vector3.left,
            Vector3.right,Vector3.right,Vector3.right,Vector3.right,
            Vector3.back, Vector3.back,Vector3.back,Vector3.back,
            Vector3.forward,Vector3.forward,Vector3.forward,Vector3.forward
        };

        m.uv = new Vector2[]
        {
            new Vector2(0,0), new Vector2(currentLevel.width, 0),new Vector2(currentLevel.width, currentLevel.depth),new Vector2(0, currentLevel.depth),
            new Vector2(0,0), new Vector2(currentLevel.width, 0),new Vector2(currentLevel.width, currentLevel.depth),new Vector2(0, currentLevel.depth),

            new Vector2(0,0), new Vector2(currentLevel.depth, 0),new Vector2(currentLevel.depth, currentLevel.height),new Vector2(0, currentLevel.height),
            new Vector2(0,0), new Vector2(currentLevel.depth, 0),new Vector2(currentLevel.depth, currentLevel.height),new Vector2(0, currentLevel.height),

            new Vector2(0,0), new Vector2(currentLevel.width, 0),new Vector2(currentLevel.width, currentLevel.height),new Vector2(0, currentLevel.height),
            new Vector2(0,0), new Vector2(currentLevel.width, 0),new Vector2(currentLevel.width, currentLevel.height),new Vector2(0, currentLevel.height)
        };

        m.triangles = new int[]
        {
            0,2,1, 0,3,2,
            4,5,6, 4,6,7,

            8,10,9, 8,11,10,
            12,13,14, 12,14,15,

            16,17,18, 16,18,19,
            20,22,21, 20,23,22
        };

        m.UploadMeshData(true);
    }
}
