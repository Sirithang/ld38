﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCreatePopup : MonoBehaviour
{
    public InputField width;
    public InputField height;
    public InputField depth;
}
